/*
 * data_structures.h
 *
 *  Created on: Sep 22, 2016
 *      Author: creutz
 */

#include "data_structures.h"
#include "math.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"

void deleteGraph(Graph *graph) {
	for (int i = 0; i < graph->size; i++) {
		free(graph->positions[i]);
	}
	free(graph->positions);
	free(graph);
}

void deleteTrip(Trip *trip) {
	for (int i = 0; i < trip->size - 1; i++) {
		free(trip->positions[i]);
	}
	free(trip->positions);
	free(trip);
}

float distance(Position *p1, Position *p2) {
	float xd = p2->x - p1->x;
	float yd = p2->y - p1->y;
	return sqrt(xd * xd + yd * yd);
}

Graph *createGraph(int size) {
	Graph *graph = malloc(sizeof(Graph));

	if (graph == NULL)
		return NULL;

	graph->positions = malloc(size * sizeof(Position));
	graph->size = size;
	return graph;
}
Position *createPosition(float x, float y) {
	Position *pos = malloc(sizeof(Position));

	if (pos == NULL)
		return NULL;

	pos->x = x;
	pos->y = y;
	return pos;
}
float tripLength(Trip *trip) {
	if (trip == NULL)
		return -1;

	float length = 0;
	for (int i = 0; i < trip->size - 1; i++) {
		length += distance(trip->positions[i], trip->positions[i + 1]);
	}

	return length;
}

Trip* createTrip(Graph *graph, int startNode) {
	if (startNode >= graph->size || startNode < 0) {
		char msg[200];
		sprintf(msg, "ERROR in createTrip: Invalid start node (%d).",
				startNode);
		perror(msg);
		exit(1);
	}

	Trip *trip = malloc(sizeof(Trip));

	if (trip == NULL)
		return NULL;

	trip->size = graph->size + 1;
	trip->positions = malloc((trip->size) * (sizeof(Position)));

	for (int i = 0; i < graph->size; i++) {
		trip->positions[i] = createPosition(graph->positions[i]->x,
				graph->positions[i]->y);
	}

	tripSwap(trip, startNode, 0);
	trip->positions[trip->size - 1] = trip->positions[0];

	return trip;
}

void tripSwap(Trip *trip, int e1, int e2) {
	if (trip == NULL)
		return;

	Position *temp = trip->positions[e1];
	trip->positions[e1] = trip->positions[e2];
	trip->positions[e2] = temp;
}

void saveGraphToFile(Graph *graph, const char* folder, const char *file) {
	char filePath[strlen(folder) + strlen(file)];
		sprintf(filePath, "%s/%s", folder, file);

	FILE *f = fopen(filePath, "w");

	if (f == NULL)
		return;

	for (int i = 0; i < graph->size; i++) {
		fprintf(f, "%f, %f\n", graph->positions[i]->x, graph->positions[i]->y);
	}

	fclose(f);
}

void saveTripToFile(Trip *trip, const char* folder, const char *file) {
	char filePath[strlen(folder) + strlen(file)];
	sprintf(filePath, "%s/%s", folder, file);

	FILE *f = fopen(filePath, "w");

	if (f == NULL)
		return;

	for (int i = 0; i < trip->size - 1; i++) {
		fprintf(f, "%f, %f\n", trip->positions[i]->x, trip->positions[i]->y);
	}

	fclose(f);
}

Trip* copyTrip(const Trip *trip) {
	Trip* copiedTrip = malloc(sizeof(Trip));

	copiedTrip->positions = malloc(trip->size * sizeof(Position));
	copiedTrip->size = trip->size;

	for (int i = 0; i < trip->size; i++) {
		Position* pos = trip->positions[i];

		copiedTrip->positions[i] = createPosition(pos->x, pos->y);
	}

	return copiedTrip;
}

void shakeupTrip(Trip *trip) {
	for (int i = 1; i < trip->size - 2; i++) {
		int randIndex = i + (rand() % (trip->size - 2 - i));
		tripSwap(trip, i, randIndex);
	}
}

