/*
 * data_structures.h
 *
 *  Created on: Sep 22, 2016
 *      Author: creutz
 */

#ifndef DATA_STRUCTURES_H_
#define DATA_STRUCTURES_H_

#include <stdlib.h>

/*
 * Position
 *
 * A struct for making edges in the two dimensional graph.
 *
 * Arguments:
 * 	@x : float
 * 	@y : float
 */
typedef struct Position {
	float x;
	float y;
} Position;


/*
 * Graph
 *
 * A graph in the cartesian plane. It consists of a list of nodes.
 * 	$Size is equal to the amount of nodes.
 *
 * Arguments:
 * 	@size : int
 * 	@positions : **Position
 */
typedef struct Graph {
	int size;
	Position **positions;
} Graph;


/*
 * Trip
 *
 * A TSP trip through the graph. The trip starts at a node and moves
 * through each other node once, before finally ending on the start node.
 * 	$Size should be equal to the amount of positions.
 *
 * Arguments:
 * 	@size : int
 * 	@positions : **Position
 */
typedef struct Trip {
	int size;
	Position **positions;
} Trip;


/*
 * deleteTrip()
 *
 * Deletes a trip and its data.
 *
 * Arguments
 * 	@trip : Trip
 */
void deleteTrip(Trip *trip);

/*
 * deleteGraph()
 *
 * Deletes a graph and its data.
 *
 * Arguments
 * 	@graph : graph
 */
void deleteGraph(Graph *graph);

/*
 * distance()
 *
 * Calculates the cartesian distance between two nodes.
 *
 * Arguments:
 * 	@p1 : *Position
 * 	@p2 : *Position
 * Return
 * 	float
 */
float distance(Position *p1, Position *p2);

/*
 * createGraph()
 *
 * Creates an empty graph with nodes equal to $size.
 *
 * Arguments:
 * 	@size : int
 * Return
 * 	*Graph
 */
Graph* createGraph(int size);

/*
 * createPosition()
 *
 * Creates a position.
 *
 * Arguments:
 * 	@x : float
 * 	@y : float
 * Return
 * 	*Position
 */
Position* createPosition(float x, float y);

/*
 * tripLength()
 *
 * Calculates the length of the trip as the sum of all distance(p[i], p[i+1]).
 *
 * Arguments
 * 	@trip : *Trip
 * Return
 * 	float
 */
float tripLength(Trip *trip);

/*
 * createTrip()
 *
 * Creates an empty trip with $graph->size positions.
 *
 * Arguments
 * 	@graph : *Graph
 * Return
 * 	*Trip
 */
Trip* createTrip(Graph *graph, int startNode);

/*
 * tripSwap()
 *
 * In a trip, it swaps two positions with indices e1 and e2.
 *
 * Arguments
 * 	@size : int
 * Return
 * 	*Trip
 */
void tripSwap(Trip *trip, int e1, int e2);

/*
 * saveGraphToFile()
 *
 * Saves a $graph to a file at $filePath, as a list of comma separated floats.
 *
 * Arguments:
 * 	@graph : *Graph
 * 	@filePath : *char
 */
void saveGraphToFile(Graph *graph, const char* folder, const char *file);

/*
 * saveTripToFile()
 *
 * Saves a $trip to a file at $filePath, as a list of comma separated floats.
 *
 * Arguments:
 * 	@graph : *Trip
 * 	@filePath : *char
 */
void saveTripToFile(Trip *trip, const char* folder, const char *filePath);

/*
 * copyTrip()
 *
 * Returns a copy of the trip as a new object.
 *
 *
 */
Trip* copyTrip(const Trip *trip);

/*
 * shakeupTrip()
 *
 * Returns a random version of the trip, with the same starting city.
 *
 * Arguments:
 * 	- @trip : *Trip
 */
void shakeupTrip(Trip *trip);

#endif /* DATA_STRUCTURES_H_ */
