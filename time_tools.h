/*
 * time_tools.h
 *
 *  Created on: Sep 25, 2016
 *      Author: creutz
 */

#ifndef TIME_TOOLS_H_
#define TIME_TOOLS_H_

#include <sys/time.h>

float timedifference_sec(struct timeval t0, struct timeval t1);

#endif /* TIME_TOOLS_H_ */
