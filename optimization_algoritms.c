/*
 * optimization_algoritms.c
 *
 *  Created on: Sep 22, 2016
 *      Author: creutz
 */

#include "optimization_algoritms.h"
#include "stdlib.h"
#include "float.h"
#include "time.h"
#include "stdio.h"
#include <sys/time.h>
#include "time_tools.h"
#include "limits.h"
#include "paths.h"

//const char* rootFolder = "/home/creutz/workspace/Optimering - Oblig 1";

Trip *greedyTrip(Graph *graph, int startNode) {
	if (graph == NULL) {
		perror("ERROR in random: Graph was null.");
		exit(1);
	}

	Trip *trip = createTrip(graph, startNode);

	if (trip == NULL) {
		perror("ERROR in random: Could not create trip.");
		exit(1);
	}

	/*
	 * At each position, the next node will be the one with the minimum distance
	 * between it and the current node.
	 *
	 * Remember that we don't change the starting node, and as such don't change
	 * the ending node (positions[size-1]) because graph->size == (trip->size+1).
	 */
	for (int i = 1; i < graph->size - 1; i++) {
		int next = -1;

		// Assume infinite distance to all other nodes.
		float lowestDist = FLT_MAX;

		// Select the next node.
		for (int k = i; k < graph->size; k++) {
			float dist = distance(trip->positions[i - 1], trip->positions[k]);
			if (dist <= lowestDist) {
				next = k;
				lowestDist = dist;
			}

		}

		// If no next node has been found, error.
		if (next == -1) {
			perror("ERROR in greedyTrip: Couldn't find next node with satisfying distance.");
			exit(1);
		}

		// Sets the next node of the one we found.
		tripSwap(trip, i, next);
	}

	return trip;
}

Trip* randomIterative(Graph *graph, int startNode, int iterations) {
	if (graph == NULL) {
		perror("ERROR in randomIterative: Graph was null.");
		exit(1);
	}

	Trip *oldTrip = randomTrip(graph, startNode);

	if (oldTrip == NULL) {
		perror("ERROR in randomIterative: Could not create oldTrip.");
		return NULL;
	}

	float oldLength = tripLength(oldTrip);

	for (int i = 0; i < iterations; i++) {
		Trip *newTrip = randomTrip(graph, startNode);

		if (newTrip == NULL) {
			perror("ERROR in randomIterative: Could not create newTrip.");
			return NULL;
		}

		float newLength = tripLength(newTrip);

		if (newLength < oldLength) {
			Trip *toRemove = oldTrip;
			oldTrip = newTrip;
			oldLength = newLength;
			deleteTrip(toRemove);
		} else {
			deleteTrip(newTrip);
		}

	}

	return oldTrip;
}

Trip* randomTrip(Graph *graph, int startNode) {
	if (graph == NULL) {
		perror("ERROR in randomTrip: Graph was null.");
		exit(1);
	}


	Trip *trip = createTrip(graph, startNode);

	if (trip == NULL) {
		perror("ERROR in randomTrip: Could not create trip.");
		exit(1);
	}

	int lastExchange = trip->size - 1;
	for (int i = 1; i < trip->size - 2; i++) {
		int exchangeStart = i + 1;

		// Selects the next random index in the area [i+1, trip->size-2].
		int randIndex = exchangeStart
				+ (rand() % (lastExchange - exchangeStart));

		tripSwap(trip, i, randIndex);
	}

	return trip;
}

void greedyHeuristic(Trip *trip, const long maxIterations, const float maxTime,
		const long maxConsec) {
// Decides which stopping criteria are used.
	int stopOnIterations = maxIterations > 0 ? 1 : 0;
	int stopOnTime = maxTime > 0 ? 1 : 0;
	int stopOnConsec = maxConsec > 0 ? 1 : 0;
	int keepGoing = stopOnConsec || stopOnIterations || stopOnTime;

	unsigned long currentIteration = 0;
	struct timeval startTime, stopTime;
	gettimeofday(&startTime, NULL);
	long consec = 0;
	float oldTripLength = tripLength(trip);
	while (keepGoing) {
		// Select two random nodes.
		int e1 = 1 + rand() % (trip->size - 2);
		int e2 = 1 + rand() % (trip->size - 2);
		while (e1 == e2) {
			e2 = 1 + rand() % (trip->size - 2);
		}

		// Try swapping the nodes.
		tripSwap(trip, e1, e2);
		float newTripLength = tripLength(trip);

		// If the new trip is better, keep it. Otherwise, reject it.
		if (newTripLength <= oldTripLength) {
			oldTripLength = newTripLength;
			consec = 0;
		} else {
			tripSwap(trip, e1, e2);
		}

		// Stopping criteria.
		if (stopOnIterations) {
			if (currentIteration >= maxIterations) {
				break;
			}
		}
		if (stopOnTime) {
			gettimeofday(&stopTime, NULL);
			if (timedifference_sec(startTime, stopTime) >= maxTime) {
				break;
			}
		}
		if (stopOnConsec) {
			if (consec++ >= maxConsec) {
				break;
			}
		}

		currentIteration++;

		if (currentIteration % 100000 == 0) {
			saveTripToFile(trip, "/home/creutz/workspace/Optimering - Oblig 1", "trip_save.txt");
		}
	}
}

Trip* greedyRandom(const Trip *trip, const long maxTries,
		float initProbabiliyOfAcceptance, float probabilityThreshold) {
	Trip* currentSolution = copyTrip(trip);
	Trip* bestSolution = copyTrip(trip);
	float oldCost = tripLength(currentSolution);
	float bestCost = oldCost;
	float newCost = oldCost;

	float probability = initProbabiliyOfAcceptance;
	do {
		for (int i = 0; i < maxTries; i++) {
			// Perturb the current solution
			int e1 = 1 + rand() % (trip->size - 2);
			int e2 = 1 + rand() % (trip->size - 2);
			while (e1 == e2) {
				e2 = 1 + rand() % (trip->size - 2);
			}
			tripSwap(currentSolution, e1, e2);
			newCost = tripLength(currentSolution);
			// DONE perturbing.

			// Greedy part
			if (newCost <= oldCost) {
				// Accept new solution, keep as currentSolution.
				oldCost = newCost;

				if (newCost < bestCost) {
					// New solution is the best so far, keep it.
					bestCost = newCost;
					deleteTrip(bestSolution);
					bestSolution = copyTrip(currentSolution);
				}
			} else {
				float rnd = rand();
				if (rnd < probability) {
					// Accept new solution, keep as currentSolution.
					oldCost = newCost;
				} else {
					// Reject new solution, return to old solution.
					tripSwap(currentSolution, e1, e2);
				}
			}
		}

		saveTripToFile(bestSolution, "/home/creutz/workspace/Optimering - Oblig 1", "trip_save.txt");

		probability *= 0.9f;
		//printf("%.30lf > %.30lf?\n", probability, probabilityThreshold);
	} while (probability > probabilityThreshold);

	deleteTrip(currentSolution);

	return bestSolution;
}
