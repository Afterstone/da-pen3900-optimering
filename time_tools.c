/*
 * time_tools.c
 *
 *  Created on: Sep 25, 2016
 *      Author: creutz
 */

#include "time_tools.h"

float timedifference_sec(struct timeval t0, struct timeval t1) {
	return ((t1.tv_sec - t0.tv_sec) + (t1.tv_usec - t0.tv_usec) / 1000000.0f);
}
