import matplotlib.pyplot as plt
import numpy

base_folder = '/home/creutz/workspace/Optimering - Oblig 1/'

name = "tripRandom.txt"
# name = "tripRandom_g_imp.txt"
name = "tripRandomIterative.txt"
# name = "tripRandomIterative_g_imp.txt"
name = "tripGreedy.txt"
# name = "tripGreedy_g_imp.txt"

with open(base_folder + name) as f:
    trip = f.read()
trip = [x.split(", ") for x in trip.split("\n") if len(x) > 0]
trip_x = [float(x[0]) for x in trip]
trip_y = [float(x[1]) for x in trip]



tot = float(len(trip))
for n in range(0, len(trip)-1):
    plt.plot([trip_x[n], trip_x[n+1]], [trip_y[n], trip_y[n+1]], color=(0, float(n)/tot, float(n)/tot))
plt.plot([trip_x[0], trip_x[-1]], [trip_y[0], trip_y[-1]], color='r')


for n in range(1, len(trip)):
    plt.plot(trip_x[n], trip_y[n], 'ro')

plt.plot(trip_x[0], trip_y[0], '-o', linewidth=10)

plt.show()