/*
 * optimization_algoritms.h
 *
 *  Created on: Sep 22, 2016
 *      Author: creutz
 */

#ifndef OPTIMIZATION_ALGORITMS_H_
#define OPTIMIZATION_ALGORITMS_H_

#include "data_structures.h"

/*
 * greedyTrip()
 *
 * Performs and returns a greedy trip through the TSP $graph.
 *
 * Arguments
 * 	@graph : Graph
 * Return
 * 	*Trip
 */
Trip *greedyTrip(Graph *graph, int startNode);

/*
 * randomIterative()
 *
 * Performs and returns a random iterative trip through the $graph.
 * $iterations amount of randomTrip() are performed.
 *
 * Arguments
 * 	@graph : Graph
 * Return
 * 	*Trip
 */
Trip* randomIterative(Graph *graph, int startNode, int iterations);

/*
 * randomTrip()
 *
 * Performs and returns a random  trip through the $graph.
 *
 * Arguments
 * 	@graph : Graph
 * Return
 * 	*Trip
 */
Trip* randomTrip(Graph *graph, int startNode);

/*
 * greedyHeuristic()
 *
 * Performs greedy improvements on the trip until one of the stopping criteria are met.
 * The stopping criteria are:
 * 	- maxIterations		Will perform up to this many iterations.
 * 	- maxTime			Will run for up to this many seconds.
 * 	- maxConsec			Will stop if there hasn't been an inprovement in this many iterations.
 *
 *
 * Arguments
 * 	@trip : *Trip
 * 	@maxIterations : long
 * 	@maxTime : float
 * 	@maxConsec : long
 */
void greedyHeuristic(Trip *trip, const long maxIterations, const float maxTime, const long maxConsec);

/*
 * greedyRandom()
 *
 * Performs the GreedyRandom optimization algorithm on the traveling salesman problem.
 *
 * The algorithm performs several rounds for-loops bounded by maxIterations until the
 *
 *
 * Arguments
 * 	- @trip : *Trip
 * 	- @maxTries : long
 * 	- @initProbabilityOfAcceptance : float
 * 	- @probabilityThreshold : float
 */
Trip* greedyRandom(const Trip *trip, const long maxTries, float initProbabiliyOfAcceptance, float probabilityThreshold);


#endif /* OPTIMIZATION_ALGORITMS_H_ */
