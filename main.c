/*
 * main.c
 *
 *  Created on: Sep 22, 2016
 *      Author: creutz
 */

#include "stdio.h"
#include "time.h"
#include "sys/time.h"
#include "stdlib.h"
#include "optimization_algoritms.h"
#include <string.h>
#include "time_tools.h"
#include "math.h"

#define GRAPH_SIZE 1000
// Greedy heuristic parameters.
#define GREEDY_HEURISTIC_MAX_ITERATIONS 0
#define GREEDY_HEURISTIC_MAX_TIME 30 //600
#define GREEDY_HEURISTIC_MAX_CONSEC 0 //pow(GRAPH_SIZE, 1.5f)
// Greedy random parameters.
#define GREEDY_RANDOM_MAX_TRIES 100000
#define GREEDY_RANDOM_INIT_PROB_ACCEPTANCE 0.90f
#define GREEDY_RANDOM_PROB_THRESHOLD 0.0000001

const char* rootFolder = "/home/creutz/workspace/Optimering - Oblig 1";

int main(int argc, char** argv) {
	time_t t;
	srand((unsigned) time(&t));

	Graph *graph = createGraph(GRAPH_SIZE);
	for (int i = 0; i < GRAPH_SIZE; i++) {
		float x = 1 + 9 * (float) rand() / (float) RAND_MAX;
		float y = 1 + 9 * (float) rand() / (float) RAND_MAX;

		graph->positions[i] = createPosition(x, y);
	}

	saveGraphToFile(graph, rootFolder, "graph.txt");

	struct timeval start, stop;
	float elapsed;

	int startNode = 1 + rand() % (graph->size - 1);
//
//	int runs = 10000;
//	double min = 999999, max = 0, avg = 0;
//	for (int i = 0; i < runs; i++) {
//		startNode = rand() % (graph->size-1);
////		Trip *trip = randomTrip(graph, startNode);
////		Trip *trip = randomIterative(graph, startNode, 5);
//		Trip *trip = greedyTrip(graph, startNode);
//
//		float temp = tripLength(trip);
//		avg += temp;
//		min = temp < min ? temp : min;
//		max = temp > max ? temp : max;
//		deleteTrip(trip);
//	}
//	printf("%.2f\n%.2f\n%.2f", min, max, avg/runs);
//	return 0;

	printf("Random trip:\n##############################\n");
	Trip *tripRandom = randomTrip(graph, startNode);
	saveTripToFile(tripRandom, rootFolder, "tripRandom.txt");
	printf("Trip length:            %f\n", tripLength(tripRandom));
	gettimeofday(&start, NULL);
	greedyHeuristic(tripRandom, GREEDY_HEURISTIC_MAX_ITERATIONS,
			GREEDY_HEURISTIC_MAX_TIME, GREEDY_HEURISTIC_MAX_CONSEC);
	gettimeofday(&stop, NULL);
	printf("Trip length (improved): %f\n", tripLength(tripRandom));
	elapsed = timedifference_sec(start, stop);
	printf("Improvement time:       %fs", elapsed);
	saveTripToFile(tripRandom, rootFolder, "tripRandom_g_imp.txt");

	printf("\n\nRandom Iterative trip:\n##############################\n");
	Trip *tripRandomIterative = randomIterative(graph, startNode, 5);
	saveTripToFile(tripRandomIterative, rootFolder, "tripRandomIterative.txt");
	printf("Trip length:            %f\n", tripLength(tripRandomIterative));
	gettimeofday(&start, NULL);
	greedyHeuristic(tripRandomIterative, GREEDY_HEURISTIC_MAX_ITERATIONS,
			GREEDY_HEURISTIC_MAX_TIME, GREEDY_HEURISTIC_MAX_CONSEC);
	gettimeofday(&stop, NULL);
	printf("Trip length (improved): %f\n", tripLength(tripRandomIterative));
	elapsed = timedifference_sec(start, stop);
	printf("Improvement time:       %fs", elapsed);
	saveTripToFile(tripRandomIterative, rootFolder, "tripRandomIterative_g_imp.txt");
	printf("\n\nGreedy trip:\n##############################\n");
	Trip *tripGreedy;
	tripGreedy = greedyTrip(graph, startNode);
	saveTripToFile(tripGreedy, rootFolder, "tripGreedy.txt");
	printf("Trip length:            %f\n", tripLength(tripGreedy));
	gettimeofday(&start, NULL);
	greedyHeuristic(tripGreedy, GREEDY_HEURISTIC_MAX_ITERATIONS,
			GREEDY_HEURISTIC_MAX_TIME, GREEDY_HEURISTIC_MAX_CONSEC);
	gettimeofday(&stop, NULL);
	printf("Trip length (improved): %f\n", tripLength(tripGreedy));
	elapsed = timedifference_sec(start, stop);
	printf("Improvement time:       %fs", elapsed);
	saveTripToFile(tripGreedy, rootFolder, "tripGreedy_g_imp.txt");

	printf("\n\nGreedyRandom trip:\n##############################\n");
	Trip *tripGreedyRandom;
	tripGreedyRandom = randomTrip(graph, startNode);
	saveTripToFile(tripGreedyRandom, rootFolder, "tripGreedy.txt");
	printf("Trip length:            %f\n", tripLength(tripGreedyRandom));
	gettimeofday(&start, NULL);
	tripGreedyRandom = greedyRandom(tripGreedyRandom, GREEDY_RANDOM_MAX_TRIES,
	GREEDY_RANDOM_INIT_PROB_ACCEPTANCE, GREEDY_RANDOM_PROB_THRESHOLD);
	gettimeofday(&stop, NULL);
	printf("Trip length (improved): %f\n", tripLength(tripGreedyRandom));
	elapsed = timedifference_sec(start, stop);
	printf("Improvement time:       %fs", elapsed);
	saveTripToFile(tripGreedyRandom, rootFolder, "tripGreedy_g_imp.txt");

	printf("\n\nGreedyRandom trip:\n##############################\n");
	tripGreedyRandom = greedyTrip(graph, startNode);
	saveTripToFile(tripGreedyRandom, rootFolder, "tripGreedy.txt");
	printf("Trip length:            %f\n", tripLength(tripGreedyRandom));
	gettimeofday(&start, NULL);
	tripGreedyRandom = greedyRandom(tripGreedyRandom, GREEDY_RANDOM_MAX_TRIES,
	GREEDY_RANDOM_INIT_PROB_ACCEPTANCE, GREEDY_RANDOM_PROB_THRESHOLD);
	gettimeofday(&stop, NULL);
	printf("Trip length (improved): %f\n", tripLength(tripGreedyRandom));
	elapsed = timedifference_sec(start, stop);
	printf("Improvement time:       %fs", elapsed);
	saveTripToFile(tripGreedyRandom, rootFolder, "tripGreedy_g_imp.txt");

	deleteGraph(graph);
//	deleteTrip(tripRandom);
//	deleteTrip(tripRandomIterative);
//	deleteTrip(tripGreedy);
//	deleteTrip(tripGreedyRandom);

	printf("\n\nDone!");

	return 0;

}

